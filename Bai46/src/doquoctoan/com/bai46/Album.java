package doquoctoan.com.bai46;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
	private String name;
	private String artist;
	private ArrayList<Song> songs;

	public Album(String name, String artist, ArrayList<Song> songs) {
		super();
		this.name = name;
		this.artist = artist;
		this.songs = songs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public ArrayList<Song> getSongs() {
		return songs;
	}

	public void setSongs(ArrayList<Song> songs) {
		this.songs = songs;
	}

	public boolean addSong(String title, double artist) {
		Song song = new Song(title, artist);
		if (finSong(song.getTitle()) != null) {
			return false;
		}
		return true;
	}

	private Song finSong(String title) {
		for (Song song : songs) {
			if (song.getTitle().equals(title)) {
				return song;
			}
		}
		return null;
	}

	public boolean addToPlayList(int track, LinkedList<Song> linkedList) {
		if (track > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean addToPlayList(String title, LinkedList<Song> linkedList) {
		Song song = finSong(title);
		if (song != null) {
			for (Song s : linkedList) {
				if (s.getTitle().equals(song.getTitle())) {
					return true;
				}
			}
		}

		return false;

	}
}
