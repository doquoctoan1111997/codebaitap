package doquoctoan.com.bai1;

import java.util.Scanner;

/*
* Bài tập: Viết chương trình bao gồm 2 functions:
- Function 1: tạo ra 1 mảng ngẫu nhiên N phần tử double < 100, N nhập từ bàn phím, hiển thị mảng đã tạo
- Function 2: xóa các phần tử có giá trị < 50 trong mảng trên, hiển thị phần tử mỗi lần bị xóa, hiển thị mảng kết quả
* */
public class Bai1 {
    public static void RanDomMang(double a[], int n) {

        for (int i = 0; i < a.length; i++) {
            double random = Math.random();
            random = random * 100;
            a[i] = random;
        }
    }

    public static void XoaPhanTu(double a[], int n, int pos) {
        if (n <= 0) {
            return;
        }
        if (pos < 0) {
            pos = 0;
        } else if (pos >= n) {
            pos = n - 1;
        }
        for (int i = pos; i < n - 1; i++) {

            a[i] = a[i + 1];
        }
    }

    public static void main(String[] args) {
        System.out.println("Nhập n=");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] a;
        a = new double[n];
        RanDomMang(a, n);
        System.out.println("các phần tử trong mảng là: ");
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
        for (int i = 0; i < n; i++) {
            if (a[i] < 50.0) {
                System.out.println("xóa phan tư " + a[i]);
                Bai1.XoaPhanTu(a, n, i);
                --n;
                --i;
            } else {
                continue;
            }
        }

        System.out.println("các phần tử trong mảng sau khi xoa là: ");
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);
        }
    }
}
